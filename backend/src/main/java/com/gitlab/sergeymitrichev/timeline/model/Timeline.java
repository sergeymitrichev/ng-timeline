package com.gitlab.sergeymitrichev.timeline.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "timeline")
public class Timeline extends AbstractEntity {

    @Column(name = "title", nullable = false)
    String title;

    @OneToMany(fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinColumn(name = "timeline_id", nullable = false)
    @OrderBy("finishedAt")
    Set<TimelineEvent> events;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "direction", columnDefinition = "smallint default 0")
    TimelineDirection direction;

    public Timeline() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<TimelineEvent> getEvents() {
        return events;
    }

    public void setEvents(Set<TimelineEvent> events) {
        this.events = events;
    }

    public TimelineDirection getDirection() {
        return direction;
    }

    public void setDirection(TimelineDirection direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "Timeline{" +
                "title='" + title + '\'' +
                ", events=" + events +
                ", direction=" + direction +
                '}';
    }
}
