package com.gitlab.sergeymitrichev.timeline.repository;

import com.gitlab.sergeymitrichev.timeline.model.Timeline;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin("*")
@RepositoryRestResource(collectionResourceRel = "content", path = "timeline")
public interface TimelineRepository extends PagingAndSortingRepository<Timeline, Long> {
}
