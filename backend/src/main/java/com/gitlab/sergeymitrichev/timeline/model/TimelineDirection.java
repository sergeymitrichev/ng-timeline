package com.gitlab.sergeymitrichev.timeline.model;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum TimelineDirection {
    HORIZONTAL,
    VERTICAL,
}
