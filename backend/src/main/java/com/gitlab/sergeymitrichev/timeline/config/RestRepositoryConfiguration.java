package com.gitlab.sergeymitrichev.timeline.config;

import com.gitlab.sergeymitrichev.timeline.model.Timeline;
import com.gitlab.sergeymitrichev.timeline.model.TimelineEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

@Configuration
public class RestRepositoryConfiguration {
    @Bean
    public RepositoryRestConfigurer repositoryRestConfigurer() {
        return new RepositoryRestConfigurer() {
            @Override
            public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config, CorsRegistry cors) {
                config.exposeIdsFor(Timeline.class);
                config.exposeIdsFor(TimelineEvent.class);
                config.setBasePath("/");
            }
        };
    }
}