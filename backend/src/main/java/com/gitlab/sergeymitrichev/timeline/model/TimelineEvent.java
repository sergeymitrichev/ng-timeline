package com.gitlab.sergeymitrichev.timeline.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "timeline_event")
public class TimelineEvent extends AbstractEntity {

    @Column(name = "started_at", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.NUMBER_INT)
    private Timestamp startedAt;

    @Column(name = "finished_at", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.NUMBER_INT)
    private Timestamp finishedAt;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "type", columnDefinition = "smallint default 0")
    private TimelineEventContentType type;

    @Column(name = "content", nullable = false)
    private String content;

    @Column(name = "shape_color", nullable = false, columnDefinition = "varchar(9) default '#013f72'")
    private String shapeColor;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "shape_type", columnDefinition = "smallint default 0")
    private TimelineEventShapeType shapeType;

    public TimelineEvent() {
    }

    public Timestamp getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Timestamp startedAt) {
        this.startedAt = startedAt;
    }

    public Timestamp getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(Timestamp finishedAt) {
        this.finishedAt = finishedAt;
    }

    public TimelineEventContentType getType() {
        return type;
    }

    public void setType(TimelineEventContentType type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getShapeColor() {
        return shapeColor;
    }

    public void setShapeColor(String shapeColor) {
        this.shapeColor = shapeColor;
    }

    public TimelineEventShapeType getShapeType() {
        return shapeType;
    }

    public void setShapeType(TimelineEventShapeType shapeType) {
        this.shapeType = shapeType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        TimelineEvent that = (TimelineEvent) o;
        return startedAt.equals(that.startedAt) &&
                finishedAt.equals(that.finishedAt) &&
                type == that.type &&
                content.equals(that.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), startedAt, finishedAt, type, content);
    }

    @Override
    public String toString() {
        return "TimelineEvent{" +
                "startedAt=" + startedAt +
                ", finishedAt=" + finishedAt +
                ", type=" + type +
                ", content='" + content + '\'' +
                '}';
    }
}
