export interface PagedParamsDto {
  /* zero based page number */
  page: number;
  /* results per page */
  size: number;
  /* ex. ?sort=title,asc&sort=id,desc */
  sort: string[];
}
