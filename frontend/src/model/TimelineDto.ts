import { TimelineEventDto } from './TimelineEventDto';

export interface TimelineDto {
  id: number;
  title: string;
  events: TimelineEventDto[];
  direction: TimelineDirection;
}

export enum TimelineDirection {
  HORIZONTAL,
  VERTICAL,
}
