import { PageDto } from './PageDto';

export interface PagedResponseDto<T> {
  _embedded: { content: T[] };
  page: PageDto;
}
