export interface PageDto {
  size?: number;
  totalElements?: number;
  totalPages?: number;
  number?: number;
}
