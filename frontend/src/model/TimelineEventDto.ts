export interface TimelineEventDto {
  startedAt: number;
  finishedAt: number;
  type: TimelineEventType;
  content: string;
  shapeType: TimelineEventShapeType;
  shapeColor: string;
}

export enum TimelineEventType {
  TEXT,
  IMAGE,
}

export enum TimelineEventShapeType {
  CIRCLE,
  RHOMBUS,
  SQUARE,
}
