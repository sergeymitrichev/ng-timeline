import { TimelineDto, TimelineDirection } from 'src/model/TimelineDto';
import { TimelineEventType } from 'src/model/TimelineEventDto';

export default (
  eventsSize: number = 5,
  direction: TimelineDirection = TimelineDirection.VERTICAL
): TimelineDto => {
  return {
    id: getRandomInteger(0, 100),
    title: getRandomText(5),
    events: [...Array(eventsSize).keys()].map((i) => ({
      startedAt: (i + Math.random() / 2) * 10000000000,
      finishedAt: (i + Math.random() / 2 + 0.5) * 10000000000,
      type: TimelineEventType.TEXT,
      content: getRandomText(2),
    })),
    direction,
  };
};

const getRandomText = (words: number): string => {
  return [...Array(words).keys()]
    .map((i) => (Math.random() + 1).toString(36).substring(5))
    .join(' ');
};

const getRandomInteger = (min: number, max: number): number => {
  return Math.round(Math.random() * (max - min)) + min;
};
