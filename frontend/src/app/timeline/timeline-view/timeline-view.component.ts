import { Component, Input, OnInit } from '@angular/core';
import { TimelineDirection, TimelineDto } from 'src/model/TimelineDto';
import {
  TimelineEventDto,
  TimelineEventShapeType,
} from 'src/model/TimelineEventDto';

@Component({
  selector: 'app-timeline-view',
  templateUrl: './timeline-view.component.html',
  styleUrls: ['./timeline-view.component.css'],
})
export class TimelineViewComponent implements OnInit {
  events!: TimelineEventDto[];
  direction!: TimelineDirection;

  @Input()
  set value(value: TimelineDto) {
    this.events = [...value.events.sort((a, b) => a.finishedAt - b.finishedAt)];
    this.direction = value.direction;
  }

  get directionCssClass(): string {
    return this.direction == TimelineDirection.VERTICAL
      ? 'vertical'
      : 'horizontal';
  }

  getShapeCssClass(shapeType: TimelineEventShapeType): string {
    let shapeCssClassPostfix = 'circle';
    switch (shapeType) {
      case TimelineEventShapeType.RHOMBUS:
        shapeCssClassPostfix = 'rhombus';
        break;
      case TimelineEventShapeType.SQUARE:
        shapeCssClassPostfix = 'square';
        break;
    }
    return `event-container-shape-${shapeCssClassPostfix}`;
  }

  constructor() {}

  ngOnInit() {}
}
