import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimelineViewComponent } from './timeline-view/timeline-view.component';
import { TimelineListComponent } from './timeline-list/timeline-list.component';
import { TimelineFormComponent } from './timeline-form/timeline-form.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule], // TODO separate forms and views
  exports: [
    TimelineFormComponent,
    TimelineListComponent,
    TimelineViewComponent,
  ],
  declarations: [
    TimelineFormComponent,
    TimelineListComponent,
    TimelineViewComponent,
  ],
})
export class TimelineModule {}
