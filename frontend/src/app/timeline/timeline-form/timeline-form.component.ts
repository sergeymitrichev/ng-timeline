import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TimelineDto } from 'src/model/TimelineDto';
import {
  TimelineEventDto,
  TimelineEventShapeType,
  TimelineEventType,
} from 'src/model/TimelineEventDto';
import { formatDate } from '@angular/common';
@Component({
  selector: 'app-timeline-form',
  templateUrl: './timeline-form.component.html',
  styleUrls: ['./timeline-form.component.css'],
})
export class TimelineFormComponent implements OnInit {
  @Input()
  value!: TimelineDto;
  form!: FormGroup;

  @Output()
  save: EventEmitter<TimelineDto> = new EventEmitter();

  get formEvents(): FormArray {
    return this.form.get('events') as FormArray;
  }

  get draftValue(): TimelineDto {
    const events = this.form.value.events
      .filter(
        ({ startedAt, finishedAt }: TimelineEventDto) => startedAt && finishedAt
      )
      .map((event: TimelineEventDto) => ({
        ...event,
        startedAt: new Date(event.startedAt).getTime(),
        finishedAt: new Date(event.finishedAt).getTime(),
      }));
    return {
      ...this.form.value,
      events,
      id: this.value.id,
    };
  }

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    const { direction, events, title } = this.value;
    this.form = this.fb.group({
      title: [title, Validators.required],
      direction: [direction, Validators.required],
      events: this.fb.array(
        events.map(
          ({ content, type, startedAt, finishedAt, shapeColor, shapeType }) =>
            this.fb.group({
              content: [content, Validators.required],
              type: [type, Validators.required],
              startedAt: [
                formatDate(startedAt, 'yyyy-MM-dd', 'en'),
                Validators.required,
              ],
              finishedAt: [
                formatDate(finishedAt, 'yyyy-MM-dd', 'en'),
                Validators.required,
              ],
              shapeColor: [shapeColor || '#013f72', Validators.required],
              shapeType: [
                shapeType || TimelineEventShapeType.CIRCLE,
                Validators.required,
              ],
            })
        )
      ),
    });
  }

  onSubmit() {
    this.save.emit(this.draftValue);
  }

  onPreview(id: number) {}

  onDelete(id: number) {}

  addEvent() {
    this.formEvents.push(
      this.fb.group({
        content: ['', Validators.required],
        type: [TimelineEventType.TEXT, Validators.required],
        startedAt: [null, Validators.required],
        finishedAt: [null, Validators.required],
      })
    );
  }

  deleteEvent(idx: number) {
    this.formEvents.removeAt(idx);
  }
}
