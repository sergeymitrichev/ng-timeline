import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { TimelineDto } from 'src/model/TimelineDto';

@Component({
  selector: 'app-timeline-list',
  templateUrl: './timeline-list.component.html',
  styleUrls: ['./timeline-list.component.css'],
})
export class TimelineListComponent implements OnInit {
  @Input()
  list!: TimelineDto[];

  @Output()
  open: EventEmitter<TimelineDto> = new EventEmitter();

  @Output()
  edit: EventEmitter<TimelineDto> = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  onOpen(event: TimelineDto) {
    this.open.emit(event);
  }

  onEdit(event: TimelineDto) {
    this.edit.emit(event);
  }
}
