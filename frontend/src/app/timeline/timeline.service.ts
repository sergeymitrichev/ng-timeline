import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, iif, Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PageDto } from 'src/model/PageDto';
import { PagedResponseDto } from 'src/model/ResponseDto';
import { TimelineDirection, TimelineDto } from 'src/model/TimelineDto';
import { switchMap, tap } from 'rxjs/operators';
import { Router } from '@angular/router';

const defaultTimeline: Partial<TimelineDto> = {
  direction: TimelineDirection.VERTICAL,
  events: [],
};

@Injectable({
  providedIn: 'root',
})
export class TimelineService {
  private apiUrl = environment.apiUrl;
  private content$: BehaviorSubject<TimelineDto[]> = new BehaviorSubject<
    TimelineDto[]
  >([]);
  private page$: BehaviorSubject<PageDto> = new BehaviorSubject<PageDto>({});
  private active$: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  selectAll = (): Observable<TimelineDto[]> => this.content$.asObservable();

  selectActive = (): Observable<TimelineDto> =>
    this.active$
      .asObservable()
      .pipe(
        switchMap((active) =>
          iif(
            () => active > 0,
            of(this.content$.value.find((t) => t.id === active)),
            of(<TimelineDto>defaultTimeline)
          )
        )
      ) as Observable<TimelineDto>;

  setActive = (id: number) => this.active$.next(id);

  constructor(private http: HttpClient, private router: Router) {}

  private set(content: TimelineDto[], page: PageDto) {
    this.content$.next(content);
    this.page$.next(page);
  }

  private setEntity(id: number, timeline?: TimelineDto) {
    const entitites: TimelineDto[] = this.content$.value.filter(
      (entity) => id != entity.id
    );
    if (timeline != null) {
      entitites.push(timeline);
    }
    this.content$.next(entitites);
  }

  fetchAll(): Observable<PagedResponseDto<TimelineDto>> {
    return this.http
      .get<PagedResponseDto<TimelineDto>>(`${this.apiUrl}/timeline`)
      .pipe(tap(({ _embedded: { content }, page }) => this.set(content, page)));
  }

  fetch(id: number): Observable<TimelineDto> {
    return this.http.get<TimelineDto>(`${this.apiUrl}/timeline/${id}`).pipe(
      tap((timeline) => this.setEntity(timeline.id, timeline)),
      tap(({ id }) => this.setActive(id))
    );
  }

  create(timeline: TimelineDto): Observable<TimelineDto> {
    return this.http
      .post<TimelineDto>(`${this.apiUrl}/timeline`, { ...timeline })
      .pipe(
        tap((timeline) => this.setEntity(timeline.id, timeline)),
        tap(({ id }) => this.setActive(id)),
        tap(({ id }) => this.router.navigate(['/', 'view', id]))
      );
  }

  update(timeline: TimelineDto): Observable<TimelineDto> {
    return this.http
      .put<TimelineDto>(`${this.apiUrl}/timeline/${timeline.id}`, {
        ...timeline,
      })
      .pipe(
        tap((timeline) => this.setEntity(timeline.id, timeline)),
        tap(({ id }) => this.setActive(id)),
        tap(({ id }) => this.router.navigate(['/', 'view', id]))
      );
  }

  delete(id: number) {
    return this.http.delete(`${this.apiUrl}/timeline/${id}`).pipe(
      tap(() => this.setEntity(id)),
      tap(() => this.setActive(-1)),
      tap(() => this.router.navigate(['/']))
    );
  }
}
