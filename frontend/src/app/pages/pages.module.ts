import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageEditComponent } from './page-edit/page-edit.component';
import { PageListComponent } from './page-list/page-list.component';
import { PageViewComponent } from './page-view/page-view.component';
import { TimelineModule } from '../timeline/timeline.module';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [CommonModule, RouterModule, TimelineModule],
  exports: [PageEditComponent, PageListComponent, PageViewComponent],
  declarations: [PageEditComponent, PageListComponent, PageViewComponent],
})
export class PagesModule {}
