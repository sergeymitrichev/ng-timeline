import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { TimelineService } from 'src/app/timeline/timeline.service';
import { TimelineDto } from 'src/model/TimelineDto';

@Component({
  selector: 'app-page-list',
  templateUrl: './page-list.component.html',
  styleUrls: ['./page-list.component.css'],
})
export class PageListComponent implements OnInit {
  timelines$: Observable<TimelineDto[]> = this.timelineService.selectAll();

  constructor(
    private router: Router,
    private timelineService: TimelineService
  ) {}

  ngOnInit() {}

  private navigateTo(...commands: any[]) {
    this.router.navigate(commands);
  }

  navigateToView(event: TimelineDto) {
    const { id } = event;
    this.timelineService.setActive(id);
    this.navigateTo('view', id);
  }

  navigateToEdit(event: TimelineDto) {
    const { id } = event;
    this.timelineService.setActive(id);
    this.navigateTo('edit', event.id);
  }
}
