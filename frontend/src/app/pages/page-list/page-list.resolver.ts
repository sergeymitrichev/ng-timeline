import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs';
import { TimelineService } from 'src/app/timeline/timeline.service';
import { PagedResponseDto } from 'src/model/ResponseDto';
import { TimelineDto } from 'src/model/TimelineDto';

@Injectable({
  providedIn: 'root',
})
export class PageListResolver
  implements Resolve<PagedResponseDto<TimelineDto>>
{
  constructor(private timelineService: TimelineService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<PagedResponseDto<TimelineDto>> {
    return this.timelineService.fetchAll();
  }
}
