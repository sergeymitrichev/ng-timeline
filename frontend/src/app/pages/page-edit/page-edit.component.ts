import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TimelineService } from 'src/app/timeline/timeline.service';
import { TimelineDto } from 'src/model/TimelineDto';

@Component({
  selector: 'app-page-edit',
  templateUrl: './page-edit.component.html',
  styleUrls: ['./page-edit.component.css'],
})
export class PageEditComponent implements OnInit {
  timeline$: Observable<TimelineDto> = this.timelineService.selectActive();

  constructor(private timelineService: TimelineService) {}

  ngOnInit() {}

  onDelete(id: number) {
    this.timelineService.delete(id).subscribe();
  }

  onSave(timeline: TimelineDto) {
    if (timeline.id) {
      this.timelineService.update(timeline).subscribe();
    } else {
      this.timelineService.create(timeline).subscribe();
    }
  }
}
