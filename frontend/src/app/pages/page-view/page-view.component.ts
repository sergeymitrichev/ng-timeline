import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { TimelineService } from 'src/app/timeline/timeline.service';
import { TimelineDto } from 'src/model/TimelineDto';

@Component({
  selector: 'app-page-view',
  templateUrl: './page-view.component.html',
  styleUrls: ['./page-view.component.css'],
})
export class PageViewComponent implements OnInit {
  timeline$: Observable<TimelineDto> = this.timelineService.selectActive();

  constructor(private timelineService: TimelineService) {}

  ngOnInit() {}

  onDelete(id: number) {
    this.timelineService.delete(id);
  }
}
