import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs';
import { TimelineService } from 'src/app/timeline/timeline.service';
import { TimelineDto } from 'src/model/TimelineDto';

@Injectable({
  providedIn: 'root',
})
export class PageViewResolver implements Resolve<TimelineDto> {
  constructor(private timelineService: TimelineService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<TimelineDto> {
    const id = route.params.id;
    return this.timelineService.fetch(id);
  }
}
