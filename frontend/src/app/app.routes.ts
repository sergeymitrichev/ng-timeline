import { Routes } from '@angular/router';
import { PageEditComponent } from './pages/page-edit/page-edit.component';
import { PageListComponent } from './pages/page-list/page-list.component';
import { PageListResolver } from './pages/page-list/page-list.resolver';
import { PageViewComponent } from './pages/page-view/page-view.component';
import { PageViewResolver } from './pages/page-view/page-view.resolver';

enum AppRoutes {
  List = '',
  TimelineView = 'view/:id',
  TimelineEdit = 'edit/:id',
  TimelineCreate = 'create',
}

export const appRoutes: Routes = [
  {
    path: '',
    component: PageListComponent,
    resolve: { PageListResolver },
  },
  {
    path: AppRoutes.TimelineView,
    component: PageViewComponent,
    resolve: { PageViewResolver },
  },
  {
    path: AppRoutes.TimelineEdit,
    component: PageEditComponent,
    resolve: { PageViewResolver }, // same resolver as in view, rename resolver
  },
  {
    path: AppRoutes.TimelineCreate,
    component: PageEditComponent,
  },
];
